<?php

/**
 * This function checks the intersection of time intervals.
 * @param timestamp  $t1
 * @param timestamp  $t2
 * @param timestamp  $t3
 * @param timestamp  $t4
 */
function is_intersection($t1,$t2,$t3,$t4){
    $left_part = max($t1,$t3);
    $right_part = min($t2,$t4);
    return ($right_part  - $left_part) >= 0;
}